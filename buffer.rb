class Buffer
  def initialize(size)
    @size = size
    @buff = []
    @buff_type = []
    @current = 0
    @wrap = false
    @before = false
    @after = false
  end
  def size()
    return @buff.length
  end
  def maxlen()
    maxlen = 0
    @buff.each do |string|
      if string.length > maxlen
        maxlen = string.length
      end
    end
    return maxlen
  end

  def push(string,type=0)
    if(string.chomp.empty?) then string = " " end
    string.split( /\r?\n/ ).each do |line|
      @buff[@current] = line
      @buff_type[@current] = type
      if(@size > 0)
        @current = (1+@current) % @size
      else
        @current = 1+@current
      end
      if(@current == 0) then @wrap = true end
    end
  end
  def yield(size,offset=0,&block)
    if(size < 0) then size = 0 end
    range = Range.new(0,@current-1).to_a
    if(@wrap)
      range = Range.new(@current,@size-1).to_a + range
    end
    range = range.last(size+offset)[0,size]
    @before = (size+offset < @buff.length)
    @after = (offset != 0 and size < @buff.length)
    if(block)
      range.each do |i|
        yield [@buff[i],@buff_type[i]]
      end
    else
      return range.collect{|r| [@buff[r],@buff_type[r]]}
    end
  end
  def has_after?()
    return @after
  end
  def has_before?()
    return @before
  end
  def clear()
    @current = 0
    @buff = []
    @buff_type = []
  end
end


