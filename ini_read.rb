require 'optparse'
require 'ostruct'
require "inifile"

class OptParse
  def parse(args)
    
    options = OpenStruct.new()
    options.inifile = nil

    opt_parser = OptionParser.new() do |opts|
      
      opts.banner = "Usage: monitor [options]"
      opts.separator ""

      opts.on( '-f', "-f ini_file", "chose a different initialization file") do |ini|
        options.inifile = ini
      end
    end
    opt_parser.parse!(args)
    return options
  end
end


class Ini_read
  attr_reader(:global,:sections)
#  attr_reader :global
#  attr_accessor :global
  def initialize()
    options = OptParse.new().parse(ARGV)
    find_ini(options.inifile)
    read_ini()
    parse_config()
    parse_global()
  end

  def find_ini(option_inifile)
    if(not option_inifile.nil?)
      @inifile = option_inifile
    elsif(ENV.has_key?('MONITOR_RC'))
      @inifile = ENV['MONITOR_RC']
    elsif(Process.uid == 0)
      @inifile = "/etc/monitor.rc"
    else
      @inifile = ENV['HOME']+"/.monitorrc"
    end
  end

  def read_ini()
    @config = IniFile.load(@inifile, :default => 'global')
    if(@config.nil?)
      puts "Initialization file not found or not readable"
      exit
    end
  end
 
  def each_section()
    return @sections
  end

  def parse_config()
    @global = {}
    @sections = {}
    @config.each_section do |section|
      sec = @config[section]
      if(section == "global")
        @global = sec
        next
      end
      if(not sec.has_key?('Type'))
        puts "Section incomplete, ignored: "+ section
        next
      elsif(not sec.has_key?('Command'))
        puts "Section incomplete, ignored: "+ section
        next
      end
      if(sec['Type'] == "continuous")
        if(not sec.has_key?('Buffer'))
          sec['Buffer'] = 1000
        end
      end
      if(sec['Type'] == "oneshot")
        if(not sec.has_key?('Periodic'))
          sec['Periodic'] = 600
        end
      end
      @sections[section] = sec
    end
  end

  def parse_global()
    if(@global.has_key?('list_size'))
      @global['list_size'] = @global['list_size'].to_i
      if(@global['list_size'] == 0)
        @global['list_size'] = 25
      end
    else
      @global['list_size'] = 25
    end
  end

end

