#!/usr/bin/env ruby

require "ncurses"
require_relative 'ini_read'
require_relative 'windows'
require_relative 'buffer'

def make_bufwins(sections,size)
  bufwins = []
  sections.each { |section_name,section|
    bufwin = Buff_Win.new(Ncurses.COLS()-size*Ncurses.COLS()/100,
                           size*Ncurses.COLS()/100,
                           section)
    bufwins.push(bufwin)
  }
  return bufwins
end

def update_buffers(bufwins)
  bufwins.each do |bufwin|
    bufwin.update()
  end
end

def redraw_all(list,bufwins,curr_bufwin, size)
  bufwins.each do |bufwin|
    bufwin.move_resize(Ncurses.COLS()-size*Ncurses.COLS()/100,size*Ncurses.COLS()/100)
  end
  list.resize(Ncurses.LINES(), size*Ncurses.COLS()/100)
  list.clear()
  list.print_list()
  list.refresh()
  curr_bufwin.refresh()
end


inistruct = Ini_read.new()
begin
  # initialize ncurses
  Ncurses.initscr
  Ncurses.start_color
  Ncurses.cbreak           # provide unbuffered input
  Ncurses.noecho           # turn off input echoing
  #Ncurses.nonl             # turn off newline translation
  #Ncurses.stdscr.intrflush(false) # turn off flush-on-interrupt
  Ncurses.stdscr.keypad(true)     # turn on keypad mode
  Ncurses.init_pair(1, Ncurses::COLOR_RED,   Ncurses::COLOR_BLACK)
  Ncurses.init_pair(10, Ncurses::COLOR_WHITE,   Ncurses::COLOR_BLACK)


  list = List_Win.new(inistruct.sections, inistruct.global['list_size'])
  bufwins = make_bufwins(inistruct.sections, inistruct.global['list_size'])
  entry = 0
  cur_bufwin = bufwins[entry]
  cur_bufwin.show_win()
  list.print_list()
  while(ch = list.getch()) do
    case(ch)
    when "n".ord
      entry = (entry +1) % bufwins.length
      cur_bufwin = bufwins[entry]
      cur_bufwin.show_win()
      list.print_list(entry=entry)
    when "p".ord
      entry = (entry -1) % bufwins.length
      cur_bufwin = bufwins[entry]
      cur_bufwin.show_win()
      list.print_list(entry=entry)
    when 12 #ctrl+L
      redraw_all(list,bufwins,cur_bufwin, inistruct.global['list_size'])
    when 18 #ctrl+R
      cur_bufwin.update(force=true)
    when Ncurses::KEY_RESIZE
      redraw_all(list,bufwins,cur_bufwin, inistruct.global['list_size'])
    when Ncurses::KEY_LEFT
      cur_bufwin.hscroll(scroll=-1)
    when Ncurses::KEY_RIGHT
      cur_bufwin.hscroll(scroll=1)
    when Ncurses::KEY_DOWN
      cur_bufwin.scroll(scroll=-1)
    when Ncurses::KEY_UP
      cur_bufwin.scroll(scroll=1)
    when Ncurses::KEY_NPAGE
      cur_bufwin.scroll(scroll=0,goto=nil,fact=-0.75)
    when Ncurses::KEY_PPAGE
      cur_bufwin.scroll(scroll=0,goto=nil,fact=0.75)
    when Ncurses::KEY_HOME
      cur_bufwin.scroll(scroll=0,goto=1.0)
    when Ncurses::KEY_END
      cur_bufwin.scroll(scroll=0,goto=0.0)
    when Ncurses::ERR
      update_buffers(bufwins)
      cur_bufwin.show_win()
    when "q".ord
      break
    else
      next
    end
  end

ensure
  Ncurses.echo
  Ncurses.nocbreak
  Ncurses.nl
  Ncurses.endwin
end
